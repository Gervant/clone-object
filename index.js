const person = {
    'name data': {
        'first name': 'Vlad',
        'last name': 'Kucherov',
        'patronymic': 'Vitalievich'
    },
    'birth data': {
        day: 17,
        month: 11,
        year: 1995,
        'zodiac': {
            name: 'scorpion',
            month: 'november',
            array: [23, 45]
        }
    }

}

function copyObject(object) {
    let newObject = {}
    for (let key in object) {
        newObject[key] = object[key]
        if (typeof object[key] === object) {
            copyObject(object[key])
        }
    }
    return newObject
}

const newObject = copyObject(person)

console.log(newObject)